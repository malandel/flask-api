define({
  "name": "Flask REST API",
  "version": "1.0.0",
  "description": "A Flask REST API example with Firebase",
  "title": "A Flask REST API example",
  "url": "http://localhost:5000",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2021-08-30T14:21:19.459Z",
    "url": "https://apidocjs.com",
    "version": "0.29.0"
  }
});

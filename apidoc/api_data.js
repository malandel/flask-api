define({ "api": [
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./apidoc/main.js",
    "group": "/home/marie/Documents/PROJECTS/flask-api/apidoc/main.js",
    "groupTitle": "/home/marie/Documents/PROJECTS/flask-api/apidoc/main.js",
    "name": ""
  },
  {
    "type": "delete",
    "url": "/v1/topics/{id}",
    "title": "Delete one presentation",
    "name": "Delete",
    "group": "api/v1/topics/{id}",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Example of a success response :",
          "content": "HTTP/1.1 200 OK\n{\n\"status\": \"Document deleted successfully !\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./flaskr/api.py",
    "groupTitle": "api/v1/topics/{id}"
  },
  {
    "type": "get",
    "url": "/v1/topics/{id}",
    "title": "Get one presentation",
    "name": "Get",
    "group": "api/v1/topics/{id}",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "veille",
            "description": "<p>The json presentation saved in the database.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example of a success response :",
          "content": "HTTP/1.1 200 OK\n{\n    \"auteur\": \"Pikachu\", \n    \"description\": \"Introduction au framework Python\", \n    \"lien\": \"https://flask.palletsprojects.com/en/2.0.x/#user-s-guide\", \n    \"liste de tags\": \"Python, Framework back, Flask\",\n    \"titre\": \"Flask\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./flaskr/api.py",
    "groupTitle": "api/v1/topics/{id}"
  },
  {
    "type": "put",
    "url": "/v1/topics/{id}",
    "title": "Edit one presentation",
    "name": "Put",
    "group": "api/v1/topics/{id}",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Example of a success response :",
          "content": "HTTP/1.1 200 OK\n{\n\"status\": \"Document updated successfully !\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./flaskr/api.py",
    "groupTitle": "api/v1/topics/{id}"
  },
  {
    "type": "get",
    "url": "/v1/topics",
    "title": "Get the list of all the presentations",
    "name": "Get",
    "group": "api/v1/topics",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "veilles",
            "description": "<p>All the presentations saved in the databaseand their Id's</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example of a success response :",
          "content": "HTTP/1.1 200 OK\n\"GUzYsjhKo13WDygE2iAJ\": {\n    \"auteur\": \"Pikachu\", \n    \"description\": \"Introduction au framework Python\", \n    \"lien\": \"https://flask.palletsprojects.com/en/2.0.x/#user-s-guide\", \n    \"liste de tags\": [\n      \"Python\", \n      \"Framework back\", \n      \"Flask\"\n    ], \n    \"titre\": \"Flask\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./flaskr/api.py",
    "groupTitle": "api/v1/topics"
  },
  {
    "type": "post",
    "url": "/v1/topics",
    "title": "Create a new presentation and save it to the database",
    "name": "Post",
    "group": "api/v1/topics",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "Object",
            "optional": false,
            "field": "JSON",
            "description": "<p>A Json in request with the following parameters :</p>"
          }
        ],
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "titre",
            "description": "<p>Title of the presentation</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "description",
            "description": "<p>Optional</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "lien",
            "description": "<p>Link to the presentation</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "auteur",
            "description": "<p>The author</p>"
          },
          {
            "group": "Parameter",
            "type": "array",
            "optional": false,
            "field": "liste_de_tags",
            "description": "<p>Optional : a list of tags</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response example:",
          "content": "HTTP/1.1 200 OK\n{\n    \"auteur\": \"Pikachu\", \n    \"description\": \"Introduction au framework Python\", \n    \"lien\": \"https://flask.palletsprojects.com/en/2.0.x/#user-s-guide\", \n    \"liste_de_tags\": [\"Python, Framework back, Flask\"],\n    \"titre\": \"Flask\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "status",
            "description": "<p>A success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example of a success response :",
          "content": "HTTP/1.1 200 OK\n{\"status\": \"Document created successfully !\"}",
          "type": "json"
        }
      ]
    },
    "filename": "./flaskr/api.py",
    "groupTitle": "api/v1/topics"
  }
] });

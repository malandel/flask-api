# Flask & Firebase APIRest

This is a small APIRest project with Flask and Firebase.
It displays the all CRUD for a list of presentations.

## Requirements && Installation
- Python 3
- [APIDocs](https://github.com/apidoc/apidoc) : ``` npm install -g apidoc ``` to install it globally, it's not a project's dependency.

- Clone the project and install its dependencies

```
git clone https://gitlab.com/malandel/flask-api.git
cd flask-api
sudo pip3 install requirements.txt
cd flaskr
export FLASK_APP=flaskr
export FLASK_ENV=development
```

## Firebase

- Create an account with Firebase : https://console.firebase.google.com/u/0/
- Create a new project and a web application
- Create a Firestore database (not a Realtime one)
- Create a new collection "veilles" and eventually add one document with the following fields : title, author, link, description (optionnal) and tags (optionnal)
- Download your Firebase Service Account Key : 
-- Click the Settings icon at the top of the dashboard
-- Click the Service Account tab (Compte de service)
-- Select Python option for Admin SDK configuration snippet, click Generate new private key, and save it as "compte_de_service_firebase.json"
:warning: This file MUST NOT end on the internet, it is your private key to connect to the database.
- Put this file in your flaskr folder and make sure it is registered in the .gitignore file.

- Run your application : ```flask run ``` (make sure you're running it from the root directory)
- Access the API's documentation from "/api/v1/"

## APIDOC

If you make changes in the API documentation, run `apidoc -i apidoc/` to generate the new page.
Check the official [APIDocs](https://apidocjs.com/) documentation.

## Testing

- Test your API with [Postman]('https://www.postman.com/) 
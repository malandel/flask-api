from flask import Blueprint, jsonify, request, redirect
from firebase_admin import credentials, firestore, initialize_app

# Connexion to database
cred = credentials.Certificate("flaskr/compte_de_service_firebase.json")
default_app = initialize_app(cred)
db = firestore.client()
all_veilles = db.collection('veilles')

api_bp = Blueprint("api", __name__, url_prefix='/api', static_url_path = "/", static_folder='../apidoc/')


@api_bp.route('/v1', methods=['GET'])
def display_doc():
    return api_bp.send_static_file('index.html')

"""
@api {get} /v1/topics Get the list of all the presentations
@apiName Get
@apiGroup api/v1/topics
@apiVersion 1.0.0


@apiSuccess {Object} veilles All the presentations saved in the databaseand their Id's

@apiSuccessExample {json} Example of a success response :
                HTTP/1.1 200 OK
                "GUzYsjhKo13WDygE2iAJ": {
                    "auteur": "Pikachu", 
                    "description": "Introduction au framework Python", 
                    "lien": "https://flask.palletsprojects.com/en/2.0.x/#user-s-guide", 
                    "liste de tags": [
                      "Python", 
                      "Framework back", 
                      "Flask"
                    ], 
                    "titre": "Flask"
                }
"""
"""
@api {post} /v1/topics Create a new presentation and save it to the database
@apiName Post
@apiGroup api/v1/topics
@apiVersion 1.0.0

@apiParam (Request body) {Object} JSON A Json in request with the following parameters : 
@apiParam {string} titre Title of the presentation
@apiParam {string} description Optional
@apiParam {string} lien Link to the presentation
@apiParam {string} auteur The author
@apiParam {array} liste_de_tags Optional : a list of tags

@apiParamExample {json} Response example:
                HTTP/1.1 200 OK
                {
                    "auteur": "Pikachu", 
                    "description": "Introduction au framework Python", 
                    "lien": "https://flask.palletsprojects.com/en/2.0.x/#user-s-guide", 
                    "liste_de_tags": ["Python, Framework back, Flask"],
                    "titre": "Flask"
                }

@apiSuccess {json} status A success message.

@apiSuccessExample {json} Example of a success response :
                HTTP/1.1 200 OK
                {"status": "Document created successfully !"}
"""
@api_bp.route('/v1/topics', methods=['GET', 'POST'])
def display_all_create():
    try:
        veilles = {}
        for data in all_veilles.stream():
                veilles[data.id] = data.to_dict()
        response = jsonify(veilles)
        response.headers.add("Access-Control-Allow-Origin", "*")
            
        if request.method == "POST":
            data = request.json
            all_veilles.add(data)
            return jsonify({"status": f"Document created successfully !"}), 200
        return response, 200
    except Exception as e:
        return f"An Error Occured: {e}"


"""
@api {get} /v1/topics/{id} Get one presentation
@apiName Get
@apiGroup api/v1/topics/{id}
@apiVersion 1.0.0

@apiSuccess {Object} veille The json presentation saved in the database.

@apiSuccessExample {json} Example of a success response :
                HTTP/1.1 200 OK
                {
                    "auteur": "Pikachu", 
                    "description": "Introduction au framework Python", 
                    "lien": "https://flask.palletsprojects.com/en/2.0.x/#user-s-guide", 
                    "liste de tags": "Python, Framework back, Flask",
                    "titre": "Flask"
                }
"""
"""
@api {put} /v1/topics/{id} Edit one presentation
@apiName Put
@apiGroup api/v1/topics/{id}
@apiVersion 1.0.0

@apiSuccessExample {json} Example of a success response :
                HTTP/1.1 200 OK
                {
                "status": "Document updated successfully !"
                }
"""
"""
@api {delete} /v1/topics/{id} Delete one presentation
@apiName Delete
@apiGroup api/v1/topics/{id}
@apiVersion 1.0.0

@apiSuccessExample {json} Example of a success response :
                HTTP/1.1 200 OK
                {
                "status": "Document deleted successfully !"
                }
"""
@api_bp.route('/v1/topics/<id>', methods=["GET", "PUT", "DELETE"])
def display_delete_update(id):
    veille = all_veilles.document(id).get()
    try:
        if request.method == "PUT":
            try:
                data = request.json
                all_veilles.document(id).update(data)
                return jsonify({"status": f"Document updated successfully !"}), 200
            except Exception as e:
                return f"An Error Occured: {e}"
        if request.method == "DELETE":
            all_veilles.document(id).delete()
            if all_veilles.document(id).get():
                return jsonify({"status": "Error, document not deleted."})
            return jsonify({"status": f"Document deleted successfully !"}), 200
        return jsonify(veille.to_dict())
    except Exception as e:
        return f"An Error Occured: {e}"
